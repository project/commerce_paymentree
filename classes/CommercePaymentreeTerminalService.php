<?php
/**
 * @file
 * A terminal service plugin for commerce_pos_terminal.
 */

use \Paymentree\Paymentree;
use \Paymentree\PaymentTransaction;
use \Paymentree\RefundTransaction;
use \Paymentree\VoidTransaction;
use \Paymentree\CommitTransaction;

/**
 * Integrate commerce_pos_terminal with php-paymentree-api.
 */
class CommercePaymentreeTerminalService implements CommercePosTerminalServiceInterface {

  /**
   * The type of the request.
   *
   * @var string
   */
  protected $requestType;

  /**
   * The type of the transaction.
   *
   * @var string
   */
  protected $transactionType;

  /**
   * A message from the terminal service.
   *
   * @var string
   */
  protected $transactionMessage;

  /**
   * The Paymentree client ID.
   *
   * @var string
   */
  protected $clientId;

  /**
   * The ID of the cashier.
   *
   * @var string
   */
  protected $cashier;

  /**
   * The ID of the register.
   *
   * @var string
   */
  protected $register;

  /**
   * The ID of the location.
   *
   * @var string
   */
  protected $location;

  /**
   * The response object.
   *
   * @var \Paymentree\ResponseInterface
   */
  protected $response;

  /**
   * A commerce payment transaction.
   *
   * @var object
   */
  protected $transaction;

  /**
   * CommercePaymentreeTerminalService constructor.
   */
  public function __construct() {
    $host = variable_get('commerce_paymentree_host');
    $port = variable_get('commerce_paymentree_port');
    Paymentree::connect($host, $port);

    $this->clientId = variable_get('commerce_paymentree_client_id');
    commerce_pos_cashier_get_current_cashier();
    $this->cashier = $GLOBALS['user']->uid;
  }

  /**
   * {@inheritdoc}
   */
  public function purchase($transaction) {
    $this->requestType = t('Purchase');
    $this->transaction = $transaction;
    $this->transactionType = 'sale';

    $paymentree_transaction = new PaymentTransaction();
    $paymentree_transaction->setClient($this->clientId)
      ->setReqTransId(commerce_paymentree_request_transaction_id($this->transaction))
      ->setCashier($this->cashier)
      ->setLocation($this->location)
      ->setRegister($this->register)
      ->setAmount($this->transaction->amount);

    $this->response = $paymentree_transaction->send();

    switch (get_class($this->response)) {
      case 'DebitResponse':
        $this->handleDebitResponse();
        break;

      case 'GiftcardResponse':
        $this->handleGiftcardResponse();
        break;

      case 'CashResponse':
        $this->handleCashResponse();
        break;

      case 'Response':
      default:
        break;

    }

    $this->handleGenericResponse();
    return $this->transaction;
  }

  /**
   * {@inheritdoc}
   */
  public function refund($transaction) {
    $this->requestType = t('Refund');
    $this->transaction = $transaction;
    $this->transactionType = 'refund';

    $paymentree_transaction = new RefundTransaction();
    $paymentree_transaction->setClient($this->clientId)
      ->setReqTransId(commerce_paymentree_request_transaction_id($this->transaction))
      ->setCashier($this->cashier)
      ->setLocation($this->location)
      ->setRegister($this->register)
      ->setAmount($this->transaction->amount);

    $this->response = $paymentree_transaction->send();

    switch (get_class($this->response)) {
      case 'DebitResponse':
        $this->handleDebitResponse();
        break;

      case 'GiftcardResponse':
        $this->handleGiftcardResponse();
        break;

      case 'CashResponse':
        $this->handleCashResponse();
        break;

      case 'Response':
      default:
        break;

    }

    $this->handleGenericResponse();
    return $this->transaction;
  }

  /**
   * {@inheritdoc}
   */
  public function void($transaction) {
    $this->requestType = t('Void');
    $this->transaction = $transaction;
    $this->transactionType = 'void';

    $paymentree_transaction = new VoidTransaction();
    $paymentree_transaction->setClient($this->clientId)
      ->setTransIdToVoid($this->transaction->remote_id)
      ->setReqTransId(commerce_paymentree_request_transaction_id($this->transaction))
      ->setCashier($this->cashier)
      ->setLocation($this->location)
      ->setRegister($this->register)
      ->setAmount($this->transaction->amount);

    $this->response = $paymentree_transaction->send();

    $this->handleVoidResponse();

    return array(
      'success' => Paymentree::isSuccessful($this->response->getResponseCode()),
      'message' => $this->response->getResponseMessage(),
    );
  }

  /**
   * React to a transaction being saved.
   *
   * Looks up autocommit settings to determine if a CommitTransaction request is
   * required.
   */
  public function saved() {
    switch ($this->response->getType()) {
      case Paymentree::RESPONSE_TYPE_DEBIT:
        $autocommit_settings = variable_get('commerce_paymentree_autocommit_credit_debit', array());
        break;

      case Paymentree::RESPONSE_TYPE_GIFTCARD:
        $autocommit_settings = variable_get('commerce_paymentree_autocommit_giftcard', array());
        break;

      case Paymentree::RESPONSE_TYPE_CASH:
        break;

      default:
        break;

    }

    if (!empty($autocommit_settings)) {
      if (in_array($this->transactionType, $autocommit_settings)) {
        $commit_transaction = new CommitTransaction(array('req_trans_id' => $this->response->getPayLinqTransactionId()));
        $commit_transaction->send();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setLocation($location_id = NULL) {
    if (!$location_id) {
      $this->location = commerce_paymentree_get_current_location();
    }
    else {
      $this->location = commerce_paymentree_get_paymentree_location_id($location_id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setRegister($register_id = NULL) {
    if ($register_id === NULL) {
      $this->register = commerce_paymentree_get_current_register();
    }
    else {
      $this->register = commerce_paymentree_get_paymentree_register_id($register_id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionMessage() {
    return $this->transactionMessage;
  }

  /**
   * Manipulate a commerce transaction based on the Paymentree response.
   */
  protected function handleDebitResponse() {

  }

  /**
   * Manipulate a commerce transaction based on the Paymentree response.
   */
  protected function handleGiftcardResponse() {

  }

  /**
   * Manipulate a commerce transaction based on the Paymentree response.
   */
  protected function handleCashResponse() {

  }

  /**
   * Manipulate a commerce transaction based on the Paymentree response.
   *
   * Differs from handleGenericResponse by making few changes to the transaction
   * unless the void was successful.
   */
  protected function handleVoidResponse() {
    $this->log();
    $this->transactionMessage = $this->response->getResponseMessage();

    if (Paymentree::isSuccessful($this->response->getResponseCode())) {
      $this->transaction->status = COMMERCE_PAYMENT_STATUS_VOID;
      $this->transaction->remote_id = $this->response->getPayLinqTransactionId();
      $this->transaction->message = $this->response->getResponseMessage();
      $this->transaction->remote_status = $this->response->getResponseCode();
    }

    $this->transaction->payload['void_response'] = Paymentree::getLastResponse();
  }

  /**
   * Manipulate a commerce transaction based on the Paymentree response.
   */
  protected function handleGenericResponse() {
    $this->log();
    $this->transactionMessage = $this->response->getResponseMessage();

    if (Paymentree::isSuccessful($this->response->getResponseCode())) {
      $this->transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      // TODO thoroughly test that this works with a variety of successful and
      // unsuccessful transactions.
      $this->transaction->amount = $this->response->getProcessedAmount();
    }
    else {
      $this->transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    }

    $this->transaction->remote_id = $this->response->getPayLinqTransactionId();
    $this->transaction->message = $this->response->getResponseMessage();
    $this->transaction->remote_status = $this->response->getResponseCode();
    $this->transaction->payload['response'] = Paymentree::getLastResponse();
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentType() {
    return $this->response->getType();
  }

  /**
   * Log the last request and response to watchdog.
   *
   * Only logs a message if request and response logging is enabled.
   */
  protected function log() {
    if (variable_get('commerce_paymentree_log_api_requests', FALSE)) {
      watchdog('commerce_paymentree', '@type request:<br/><br/>Request:<br/><pre>@request</pre><br/><br/>Response:<br/><pre>@response</pre>', array(
        '@type' => $this->requestType,
        '@request' => Paymentree::getLastRequest(),
        '@response' => Paymentree::getLastResponse(),
      ), WATCHDOG_DEBUG);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getPaymentTypes() {
    return array(
      Paymentree::RESPONSE_TYPE_DEBIT => t('Debit'),
      Paymentree::RESPONSE_TYPE_GIFTCARD => t('Gift card'),
      Paymentree::RESPONSE_TYPE_CASH => t('Cash'),
      Paymentree::RESPONSE_TYPE_GENERIC => t('Any other type'),
    );
  }

}
