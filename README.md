# commerce_paymentree
Commerce Paymentree allows you to connect
[Commerce Point of Sale](https://www.drupal.org/project/commerce_pos)
to your semi-integrated payment terminals through
[Paymentree](http://www.paymentree.ca).

## Features
* Provides a terminal service implementation for commerce_pos_terminal.
* Configure how to connect to Paymentree, auto-commit options, and debugging.

## Documentation
See the Paymentree User Guide for more information.

## Requirements
* Commerce Point of Sale (7.x-2.x).
* Composer manager
* php-paymentree-api

## Developed by Acro Media
* https://www.acromediainc.com/
* https://www.drupal.org/acro-media-inc
