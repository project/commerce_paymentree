<?php
/**
 * @file
 * Configuration forms for Paymentree.
 */

use \Paymentree\Paymentree;

/**
 * Configuration for Paymentree.
 */
function commerce_paymentree_settings_form($form, &$form_state) {

  $form['account']['commerce_paymentree_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Paymentree Client ID'),
    '#description' => t('Your Client ID for Paymentree'),
    '#default_value' => variable_get('commerce_paymentree_client_id'),
  );

  $form['connection']['commerce_paymentree_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Paymentree Host'),
    '#description' => t('The IP address or host name of the Paymentree instance.'),
    '#default_value' => variable_get('commerce_paymentree_host'),
  );

  $form['connection']['commerce_paymentree_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Paymentree Port'),
    '#description' => t('The port for the Paymentree instance.'),
    '#default_value' => variable_get('commerce_paymentree_port'),
  );

  $paymentree_available = class_exists('\Paymentree\Paymentree');
  if ($paymentree_available) {
    // Set default values based on Paymentree's defaults.
    if (empty($form['connection']['commerce_paymentree_host']['#default_value'])) {
      $form['connection']['commerce_paymentree_host']['#default_value'] = Paymentree::DEFAULT_LOCAL_IP_ADDRESS;
    }

    if (empty($form['connection']['commerce_paymentree_port']['#default_value'])) {
      $form['connection']['commerce_paymentree_port']['#default_value'] = Paymentree::DEFAULT_PORT;
    }

  }
  else {
    drupal_set_message(t('Unable to load php-paymentree-api.'), 'error');
  }

  $form['commit'] = array(
    '#type' => 'fieldset',
    '#title' => t('Auto-Commit Transaction'),
    '#description' => t("These settings should match Paymentree's Commit Setup configuration."),
  );

  $autocommit_options = array(
    'sale' => t('Sale'),
    'refund' => t('Refund'),
    'void' => t('Void'),
  );

  $form['commit']['commerce_paymentree_autocommit_credit_debit'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Credit and debit'),
    '#options' => $autocommit_options,
    '#default_value' => variable_get('commerce_paymentree_autocommit_credit_debit'),
  );

  $form['commit']['commerce_paymentree_autocommit_giftcard'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Gift Card'),
    '#options' => $autocommit_options,
    '#default_value' => variable_get('commerce_paymentree_autocommit_giftcard'),
  );

  $form['commit']['commerce_paymentree_autocommit_loyalty'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Loyalty'),
    '#options' => $autocommit_options,
    '#default_value' => variable_get('commerce_paymentree_autocommit_loyalty'),
  );

  $form['debugging'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debugging'),
  );

  $form['debugging']['commerce_paymentree_log_api_requests'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log API requests and responses'),
    '#default_value' => variable_get('commerce_paymentree_log_api_requests'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validation handler for commerce_paymentree_settings_form().
 */
function commerce_paymentree_settings_form_validate($form, &$form_state) {
  if (isset($form_state['values']['commerce_paymentree_port'])) {
    $port = $form_state['values']['commerce_paymentree_port'];
    if (!is_numeric($port) || $port < 1 || $port > 65535 || (float) $port !== floor($port)) {
      form_set_error('commerce_paymentree_port', t('Paymentree Port must be an integer in the range 1 to 65535.'));
    }
  }
}

/**
 * Submit handler for commerce_paymentree_settings_form().
 */
function commerce_paymentree_settings_form_submit($form, &$form_state) {
  if (isset($form_state['values']['commerce_paymentree_client_id'])) {
    variable_set('commerce_paymentree_client_id', $form_state['values']['commerce_paymentree_client_id']);
  }

  if (isset($form_state['values']['commerce_paymentree_host'])) {
    variable_set('commerce_paymentree_host', $form_state['values']['commerce_paymentree_host']);
  }

  if (isset($form_state['values']['commerce_paymentree_port'])) {
    variable_set('commerce_paymentree_port', $form_state['values']['commerce_paymentree_port']);
  }

  if (isset($form_state['values']['commerce_paymentree_log_api_requests'])) {
    variable_set('commerce_paymentree_log_api_requests', $form_state['values']['commerce_paymentree_log_api_requests']);
  }

  variable_set('commerce_paymentree_autocommit_credit_debit', $form_state['values']['commerce_paymentree_autocommit_credit_debit']);
  variable_set('commerce_paymentree_autocommit_giftcard', $form_state['values']['commerce_paymentree_autocommit_giftcard']);
  variable_set('commerce_paymentree_autocommit_loyalty', $form_state['values']['commerce_paymentree_autocommit_loyalty']);
}
